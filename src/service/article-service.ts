import type { Article } from "@/entities";
import axios from "axios";



export async function fetchAllArticles() {
    const response = await axios.get<Article[]>('http://localhost:8080/api/article');
    return response.data;
}

export async function fetchOneArticle(id: any) {
    const response = await axios.get<Article>("http://localhost:8080/api/article/" + id);
    return response.data;
}

export async function fetchAllArticleByCategorie(id: any) {
    const response = await axios.get<Article[]>("http://localhost:8080/api/article/sort/" + id);
    return response.data;
}

export async function createArticle(article: Article) {
    const response = await axios.post<Article>("http://localhost:8080/api/article", article);
    return response.data;
}

export async function updateArticle(article: Article) {
    const response = await axios.put<Article>("http://localhost:8080/api/article/" + article.id, article);
    return response.data;
}

export async function deleteArticle(id: any) {
    const response = await axios.delete<Article>("http://localhost:8080/api/article/" + id);
    return response.data;
}


export async function addLike(id: any) {
    const response = await axios.put<Article>("http://localhost:8080/api/article/" + id + "/like");
    return response.data;
}

export async function addDisLike(id: any) {
    const response = await axios.put<Article>("http://localhost:8080/api/article/" + id + "/dislike");
    return response.data;
}