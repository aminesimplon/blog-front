import type { Commentaire } from "@/entities";
import axios from "axios";



export async function fetchAllCommentairesbyArticleId(id: any) {
    const response = await axios.get<Commentaire[]>('http://localhost:8080/api/commentaire/' + id);
    return response.data;
}

export async function fetchOneCommentaire(id: any) {
    const response = await axios.get<Commentaire>("http://localhost:8080/api/commentaire/id/" + id);
    return response.data;
}


export async function createCommentaire(Commentaire: Commentaire) {
    const response = await axios.post<Commentaire>("http://localhost:8080/api/commentaire", Commentaire);
    return response.data;
}

export async function updateCommentaire(comm : Commentaire) {
    const response = await axios.put<Commentaire>("http://localhost:8080/api/commentaire", comm);
    return response.data;
}

export async function deleteCommentaire(id: any) {
    const response = await axios.delete<Commentaire>("http://localhost:8080/api/commentaire/" + id);
    return response.data;
}