export interface Article {
    id?: number;
    titre: string;
    contenu: string;
    auteur: string;
    date: string;
    image: string;
    likes: number;
    dislikes: number;
    categorieId: number;
}

export interface Categorie {
    id?: number;
    nom: string;
}


export interface Commentaire {
    id?: number;
    contenu: String;
    auteur: String;
    date: String;
    articleId: number;
}

export interface User {
    id?:number;
    email:string;
    password?:string;
    pseudo: string;
    role?:string;
}