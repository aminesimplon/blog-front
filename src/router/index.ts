import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import ArticleView from '@/views/ArticleView.vue'
import SingleArticleView from '@/views/SingleArticleView.vue'
import ArticleByCategorie from '@/views/ArticleByCategorie.vue'
import ArticleForm from '@/views/AddArticle.vue'
import LoginView from '@/views/LoginView.vue'
import NotFound from '@/views/NotFound.vue';
import CreateAccountView from '@/views/CreateAccountView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: HomeView
    },
    {
      path: '/articles',
      component: ArticleView
    },
    {
      path: '/article/:id',
      component: SingleArticleView
    },
    {
      path: '/sort/:id',
      component: ArticleByCategorie
    },
    {
      path: '/article/form',
      component: ArticleForm
    },
    {
      path: '/login',
      component: LoginView
    },
    {
      path: '/:pathMatch(.*)*',
      component: NotFound
    },
    {
      path: '/create-account',
      component: CreateAccountView
    },
  ]
})

export default router
